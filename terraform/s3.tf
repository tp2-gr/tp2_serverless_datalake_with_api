# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-khgmr"
  force_destroy = true
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.b.bucket
  key    = "job_offers/raw"
  source = "/dev/null"
}

# Create an event to trigger️ the lambda when a csv file is uploaded on the job_offers/raw bucket

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.b.bucket

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
